.386
.MODEL flat,STDCALL

STD_INPUT_HANDLE equ -10
STD_OUTPUT_HANDLE equ -11

GetStdHandle PROTO :DWORD
ExitProcess PROTO : DWORD
wsprintfA PROTO C :VARARG
WriteConsoleA PROTO : DWORD, :DWORD, :DWORD, :DWORD, :DWORD
ReadConsoleA  PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD
.DATA
	
	write dd ?
	read dd ?

	source DB 10 dup(?)
	destination DB 10 dup(?)

	bufor dd 200 DUP(?)
	buforZ dd 200 DUP(?)

	lznakow DW 0
	lznakowZ dd  0
	tekst db "Wprowadz liczbe numer  :",10,0
	tekst2 db "Wyswietlanie: ",10


	rozmiart2 dd $ - tekst2
	rozmiar dd $ - tekst
	licznik dd 1
	tmp dd ?
	

.CODE
main proc


	invoke GetStdHandle, STD_INPUT_HANDLE
	mov read, EAX

	invoke GetStdHandle, STD_OUTPUT_HANDLE
	mov write, EAX


	mov eax, LENGTHOF tekst
	sub eax,1d
	mov rozmiar,eax

	mov ECX, 10
	mov EDI, OFFSET source
	Pentela:
		push ECX
		push licznik
		push EDI
		
		

		invoke WriteConsoleA, write, OFFSET tekst, rozmiar, OFFSET lznakow, 0
	
		invoke ReadConsoleA, read, OFFSET buforZ, 10, OFFSET lznakowZ, 0
		mov EBX, OFFSET buforZ
		add EBX, lznakowZ
		mov [EBX - 2],BYTE PTR 0

		mov EAX, buforZ
		

		pop EDI
		stosb

		pop licznik
		inc licznik

		pop ECX
	loop Pentela





	mov ECX, LENGTHOF source
	mov ESI, OFFSET source
	mov EDI, OFFSET destination
	kopiowanie:

		push ECX
		mov EAX, 0

		lodsb
		stosb
		
	

		pop ECX
	loop kopiowanie

	mov ECX, LENGTHOF destination
	mov ESI, OFFSET destination
	wyswietlanie:
		push ECX
		xor EAX,EAX
		lodsb
	
		mov tmp, EAX
		mov rozmiar, LENGTHOF tmp
		invoke WriteConsoleA, write, OFFSET tmp, rozmiar, OFFSET lznakow, 0

		pop ECX

	loop wyswietlanie



		invoke ExitProcess, 0

main endp

END